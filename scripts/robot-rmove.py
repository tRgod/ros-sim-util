#!/usr/bin/env python

import numpy as np
from numpy import sin,cos

def deg2rad(degree):
    return degree*2*np.pi/360


class RobotArm:
    def __init__(self):
        pass

    def forward(self, qs):
        d1 = 400
        d4 = 420
        d6 = 80

        a1 = 25
        a2 = 455
        a3 = 35
        
        A = np.array([
        [
            [cos(qs[0]),  0,  sin(qs[0]), a1*cos(qs[0])],
            [sin(qs[0]),  0, -cos(qs[0]), a1*sin(qs[0])],
            [         0,  1,           0,            d1],
            [         0,  0,           0,             1]
        ],[
            [cos(qs[1]), -sin(qs[1]), 0, a2*cos(qs[1])],
            [sin(qs[1]),  cos(qs[1]), 0, a2*sin(qs[1])],
            [         0,           0, 1,             0],
            [         0,           0, 0,             1]
        ],[
            [cos(qs[2]),  0,  sin(qs[2]), a3*cos(qs[2])],
            [sin(qs[2]),  0, -cos(qs[2]), a3*sin(qs[2])],
            [         0,  1,           0,             0],
            [         0,  0,           0,             1]
        ],[
            [cos(qs[3]),  0, -sin(qs[3]),  0],
            [sin(qs[3]),  0,  cos(qs[3]),  0],
            [         0, -1,           0, d4],
            [         0,  0,           0,  1]
        ],[
            [cos(qs[4]),  0,  sin(qs[4]),  0],
            [sin(qs[4]),  0, -cos(qs[4]),  0],
            [         0,  1,           0,  0],
            [         0,  0,           0,  1]
        ],[
            [cos(qs[5]), -sin(qs[5]),  0,  0],
            [sin(qs[5]),  cos(qs[5]),  0,  0],
            [         0,           0,  1, d6],
            [         0,           0,  0,  1]
        ]
        ])

        pos = np.zeros((7,4,4))

        pos[0] = np.identity(4)
        #print pos
        #print A

        for i in range(0, 6):
            #print  [pos[i], A[i], np.matmul(pos[i], A[i])]
            pos[i+1] = np.matmul(pos[i], A[i])

        return pos

if __name__ == "__main__":
    
    robot = RobotArm()
    #print robot.forward([-1.47,-1.47,1.47,1.47,-1.47,0.0])
    print robot.forward([0.0,0.0,0.0,0.0,0.0,0.0])