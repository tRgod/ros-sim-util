#! /usr/bin/env python
import time 
import rospy
import numpy as np 
from robot_util.msg import Qvec
from rospy.numpy_msg import numpy_msg
from std_msgs.msg import Float64
from control_msgs.msg import JointControllerState
import message_filters

def Callback(joint1,joint2,joint3,joint4,joint5,joint6):
    pub1=rospy.Publisher('/Joint/states',Qvec,queue_size=1)
    vec=[0,0,0,0,0,0]
    vec[0]=joint1.set_point
    vec[1]=joint2.set_point
    vec[2]=joint3.set_point
    vec[3]=joint4.set_point
    vec[4]=joint5.set_point
    vec[5]=joint6.set_point
    pub1.publish(vec)
     
def Subscriber():
    rospy.init_node('Robot_pose_Util_Q')
    joint1=message_filters.Subscriber("/joint_a1_position_controller/state",JointControllerState)
    joint2=message_filters.Subscriber("/joint_a2_position_controller/state",JointControllerState)
    joint3=message_filters.Subscriber("/joint_a3_position_controller/state",JointControllerState)
    joint4=message_filters.Subscriber("/joint_a4_position_controller/state",JointControllerState)
    joint5=message_filters.Subscriber("/joint_a5_position_controller/state",JointControllerState)
    joint6=message_filters.Subscriber("/joint_a6_position_controller/state",JointControllerState)

    ts=message_filters.TimeSynchronizer([joint1,joint2,joint3,joint4,joint5,joint6],1)
    ts.registerCallback(Callback)
    rospy.spin()


if __name__=="__main__":
    try:
        Subscriber()
    except rospy.ROSInterruptException:
        pass