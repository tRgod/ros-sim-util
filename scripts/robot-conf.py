#!/usr/bin/env python

from robot_util.srv import RobotConf,RobotConfResponse
from gazebo_msgs.srv import *
1

def handle_robot_conf(req):
    print "Returning robot configuration"

    qs = [0.,0.,0.,0.,0.,0.]

    try:
        qsrv = rospy.ServiceProxy('/gazebo/get_joint_properties', GetJointProperties)
        for i in range(0,6):
            resp = qsrv('joint_a%s'%(i+1))
            qs[i] = resp.position[0]
            print resp.position
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

    return RobotConfResponse(qs)

def robot_conf_server():
    rospy.init_node('robot_conf')
    s = rospy.Service('robot_conf', RobotConf, handle_robot_conf)
    print "Ready to fetch robot configuration."
    rospy.spin()

if __name__ == "__main__":
    robot_conf_server()